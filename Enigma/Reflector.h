#pragma once
#include "Rotor.h"
namespace ven {
	namespace enigma {
		class Reflector : private Rotor
		{
			private:

			protected:
				Reflector(const std::string& transform);
				Reflector(const char* transform);

			public:

				static const Reflector UKW_R;
				static const Reflector UKW_K;
				static const Reflector A;
				static const Reflector B;
				static const Reflector C;
				static const Reflector B_Thin;
				static const Reflector C_Thin;

				//~Reflector();
				letter transform(const letter input) const;
		};
	};
};

