#include "Reflector.h"

const ven::enigma::Reflector ven::enigma::Reflector::UKW_R("QYHOGNECVPUZTFDJAXWMKISRBL");
const ven::enigma::Reflector ven::enigma::Reflector::UKW_K("IMETCGFRAYSQBZXWLHKDVUPOJN");
const ven::enigma::Reflector ven::enigma::Reflector::A("EJMZALYXVBWFCRQUONTSPIKHGD");
const ven::enigma::Reflector ven::enigma::Reflector::B("YRUHQSLDPXNGOKMIEBFZCWVJAT");
const ven::enigma::Reflector ven::enigma::Reflector::C("FVPJIAOYEDRZXWGCTKUQSBNMHL");
const ven::enigma::Reflector ven::enigma::Reflector::B_Thin("ENKQAUYWJICOPBLMDXZVFTHRGS");
const ven::enigma::Reflector ven::enigma::Reflector::C_Thin("RDOBJNTKVEHMLFCWZAXGYIPSUQ");


ven::enigma::Reflector::Reflector(const std::string& transform) :
	Rotor(transform, "")
{}
ven::enigma::Reflector::Reflector(const char* transform) :
	Rotor(transform, "")
{}
//ven::enigma::Reflector::~Reflector(){}
ven::enigma::letter ven::enigma::Reflector::transform(const ven::enigma::letter input) const
{
	return Rotor::transformFoward(input);
}
