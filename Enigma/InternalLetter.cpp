#include "InternalLetter.h"


const size_t ven::enigma::letterSizeT = 26;
const char ven::enigma::letterSize = 26;


ven::enigma::letter& ven::enigma::operator++(ven::enigma::letter& orig) {
	orig += 1;
	return orig;
}
ven::enigma::letter ven::enigma::operator++(ven::enigma::letter& orig, int) {
	letter res = orig;
	orig += 1;
	return res;
}
ven::enigma::letter& ven::enigma::operator+=(ven::enigma::letter& orig, int val) {
	using namespace ven::enigma;
	size_t value = getSize_T(orig);
	value += val;
	value %= letterSizeT;
	orig = get(value);
	return orig;
}
ven::enigma::letter& ven::enigma::operator+=(ven::enigma::letter& orig, const ven::enigma::letter& val) {
	using namespace ven::enigma;
	size_t value = getSize_T(orig);
	value += getSize_T(val);
	value %= letterSizeT;
	orig = get(value);
	return orig;
}
ven::enigma::letter ven::enigma::operator+ (const ven::enigma::letter& op1, const ven::enigma::letter& op2)
{
	auto res = op1;
	return res += op2;
}
ven::enigma::letter& ven::enigma::operator--(ven::enigma::letter& orig) {
	orig -= 1;
	return orig;
}
ven::enigma::letter ven::enigma::operator--(ven::enigma::letter& orig, int) {
	letter res = orig;
	orig -= 1;
	return res;
}
ven::enigma::letter& ven::enigma::operator-=(ven::enigma::letter& orig, int val) {
	using namespace ven::enigma;
	size_t value = getSize_T(orig) + letterSizeT;
	value -= val % letterSizeT;
	value %= letterSizeT;
	orig = get(value);
	return orig;
}
ven::enigma::letter& ven::enigma::operator-=(ven::enigma::letter& orig, const ven::enigma::letter& val) {
	using namespace ven::enigma;
	size_t value = getSize_T(orig) + letterSizeT;
	value -= getSize_T(val) % letterSizeT;
	value %= letterSizeT;
	orig = get(value);
	return orig;
}
ven::enigma::letter ven::enigma::operator- (const ven::enigma::letter& op1, const ven::enigma::letter& op2)
{
	auto res = op1;
	return res -= op2;
}

ven::enigma::letter ven::enigma::get(const char letter) {
	using namespace ven::enigma;
	switch (letter)
	{
		case 'A':
			return A;
		case 'B':
			return B;
		case 'C':
			return C;
		case 'D':
			return D;
		case 'E':
			return E;
		case 'F':
			return F;
		case 'G':
			return G;
		case 'H':
			return H;
		case 'I':
			return I;
		case 'J':
			return J;
		case 'K':
			return K;
		case 'L':
			return L;
		case 'M':
			return M;
		case 'N':
			return N;
		case 'O':
			return O;
		case 'P':
			return P;
		case 'Q':
			return Q;
		case 'R':
			return R;
		case 'S':
			return S;
		case 'T':
			return T;
		case 'U':
			return U;
		case 'V':
			return V;
		case 'W':
			return W;
		case 'X':
			return X;
		case 'Y':
			return Y;
		case 'Z':
			return Z;
		default:
			return A;
	}
}
char ven::enigma::get(const ven::enigma::letter let) {
	using namespace ven::enigma;
	switch (let)
	{
		case A:
			return 'A';
		case B:
			return 'B';
		case C:
			return 'C';
		case D:
			return 'D';
		case E:
			return 'E';
		case F:
			return 'F';
		case G:
			return 'G';
		case H:
			return 'H';
		case I:
			return 'I';
		case J:
			return 'J';
		case K:
			return 'K';
		case L:
			return 'L';
		case M:
			return 'M';
		case N:
			return 'N';
		case O:
			return 'O';
		case P:
			return 'P';
		case Q:
			return 'Q';
		case R:
			return 'R';
		case S:
			return 'S';
		case T:
			return 'T';
		case U:
			return 'U';
		case V:
			return 'V';
		case W:
			return 'W';
		case X:
			return 'X';
		case Y:
			return 'Y';
		case Z:
			return 'Z';
	}
	return 'A';
}
ven::enigma::letter ven::enigma::get(const size_t letter) {
	using namespace ven::enigma;
	return get(static_cast<char>(letter % letterSizeT + 'A'));
}
size_t ven::enigma::getSize_T(const ven::enigma::letter let) {
	using namespace ven::enigma;
	return static_cast<size_t>(get(let) - 'A');
}