#pragma once
namespace ven{
	namespace enigma {
		enum letter : char {
			A = 0,
			B,
			C,
			D,
			E,
			F,
			G,
			H,
			I,
			J,
			K,
			L,
			M,
			N,
			O,
			P,
			Q,
			R,
			S,
			T,
			U,
			V,
			W,
			X,
			Y,
			Z
		};
		letter& operator++(letter& orig);
		letter operator++(letter& orig, int);
		letter& operator+=(letter& orig, int val);
		letter& operator+=(letter& orig, const letter& val);
		letter operator+ (const letter& op1, const letter& op2);
		letter& operator--(letter& orig);
		letter operator--(letter& orig, int);
		letter& operator-=(letter& orig, int val);
		letter& operator-=(letter& orig, const letter& val);
		letter operator- (const letter& op1, const letter& op2);

		extern const size_t letterSizeT;
		extern const char letterSize;
		letter get(const char let);
		char get(const letter let);
		letter get(const size_t num);
		size_t getSize_T(const letter let);
	};
};
