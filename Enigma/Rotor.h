#pragma once

#include <map>
#include <set>
#include <string>

#include "InternalLetter.h"

namespace ven{
	namespace enigma {
		class Rotor
		{
			private:
				std::map<ven::enigma::letter, ven::enigma::letter> _fowardTransform;
				std::map<ven::enigma::letter, ven::enigma::letter> _backwardTransform;

				std::set<ven::enigma::letter> _turnOverPositions;

				ven::enigma::letter _internalPosition;

				ven::enigma::letter internalTransformFoward(const ven::enigma::letter input) const;
				ven::enigma::letter internalTransformBackward(const ven::enigma::letter input) const;
			protected:

				Rotor(const std::string& transform, const std::string& turnOver);
				Rotor(const char* transform, const char* turnOver);
			public:
				// Commercial
				static const Rotor IC;
				static const Rotor IIC;
				static const Rotor IIIC;
				// Railway
				static const Rotor I_R;
				static const Rotor II_R;
				static const Rotor III_R;
				static const Rotor ETW_R;
				// Swiss
				static const Rotor I_K;
				static const Rotor II_K;
				static const Rotor III_K;
				static const Rotor ETW_K;
				// Wehrmacht
				static const Rotor I;
				static const Rotor II;
				static const Rotor III;
				static const Rotor IV;
				static const Rotor V;
				static const Rotor VI;
				static const Rotor VII;
				static const Rotor VIII;
				// Speacials
				static const Rotor Beta;
				static const Rotor Gamma;
				static const Rotor ETW;


				ven::enigma::letter transformFoward(const ven::enigma::letter input) const;
				ven::enigma::letter transformBackward(const ven::enigma::letter input) const;
				bool advance();
				bool isAtTurnOverPosition() const;


				//~Rotor();
		};
	};
};

