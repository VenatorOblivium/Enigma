#include "Rotor.h"

#include <exception>

// Commercial
const ven::enigma::Rotor ven::enigma::Rotor::IC("DMTWSILRUYQNKFEJCAZBPGXOHV", "Q");
const ven::enigma::Rotor ven::enigma::Rotor::IIC("HQZGPJTMOBLNCIFDYAWVEUSRKX", "E");
const ven::enigma::Rotor ven::enigma::Rotor::IIIC("UQNTLSZFMREHDPXKIBVYGJCWOA", "V");
// Railway
const ven::enigma::Rotor ven::enigma::Rotor::I_R("JGDQOXUSCAMIFRVTPNEWKBLZYH", "Q");
const ven::enigma::Rotor ven::enigma::Rotor::II_R("NTZPSFBOKMWRCJDIVLAEYUXHGQ	", "E");
const ven::enigma::Rotor ven::enigma::Rotor::III_R("JVIUBHTCDYAKEQZPOSGXNRMWFL", "V");
const ven::enigma::Rotor ven::enigma::Rotor::ETW_R("QWERTZUIOASDFGHJKPYXCVBNML", "");
// Swiss
const ven::enigma::Rotor ven::enigma::Rotor::I_K("PEZUOHXSCVFMTBGLRINQJWAYDK", "Q");
const ven::enigma::Rotor ven::enigma::Rotor::II_K("ZOUESYDKFWPCIQXHMVBLGNJRAT	", "E");
const ven::enigma::Rotor ven::enigma::Rotor::III_K("EHRVXGAOBQUSIMZFLYNWKTPDJC", "V");
const ven::enigma::Rotor ven::enigma::Rotor::ETW_K("QWERTZUIOASDFGHJKPYXCVBNML", "");
// Wehrmacht
const ven::enigma::Rotor ven::enigma::Rotor::I("EKMFLGDQVZNTOWYHXUSPAIBRCJ","Q");
const ven::enigma::Rotor ven::enigma::Rotor::II("AJDKSIRUXBLHWTMCQGZNPYFVOE", "E");
const ven::enigma::Rotor ven::enigma::Rotor::III("BDFHJLCPRTXVZNYEIWGAKMUSQO", "V");
const ven::enigma::Rotor ven::enigma::Rotor::IV("ESOVPZJAYQUIRHXLNFTGKDCMWB", "J");
const ven::enigma::Rotor ven::enigma::Rotor::V("VZBRGITYUPSDNHLXAWMJQOFECK", "Z");
const ven::enigma::Rotor ven::enigma::Rotor::VI("JPGVOUMFYQBENHZRDKASXLICTW", "ZM");
const ven::enigma::Rotor ven::enigma::Rotor::VII("NZJHGRCXMYSWBOUFAIVLPEKQDT", "ZM");
const ven::enigma::Rotor ven::enigma::Rotor::VIII("FKQHTLXOCBJSPDZRAMEWNIUYGV", "ZM");
// Specials
const ven::enigma::Rotor ven::enigma::Rotor::Beta("LEYJVCNIXWPBQMDRTAKZGFUHOS", "");
const ven::enigma::Rotor ven::enigma::Rotor::Gamma("FSOKANUERHMBTIYCWLQPZXVGJD", "");
const ven::enigma::Rotor ven::enigma::Rotor::ETW("ABCDEFGHIJKLMNOPQRSTUVWXYZ", "");


ven::enigma::letter ven::enigma::Rotor::internalTransformFoward(const ven::enigma::letter input) const
{
	return _fowardTransform.at(input);
}
ven::enigma::letter ven::enigma::Rotor::internalTransformBackward(const ven::enigma::letter input) const
{
	return _backwardTransform.at(input);
	return internalTransformFoward(input + _internalPosition) - _internalPosition;
}

ven::enigma::Rotor::Rotor(const std::string& transform, const std::string& turnOver) {
	for (size_t i = 0, size = transform.size(); i < size; ++i)
	{
		_fowardTransform[get(i)] = get(transform[i]);
		_backwardTransform[get(transform[i])] = get(i);
	}
	if (_fowardTransform.size() != ven::enigma::letterSizeT || _backwardTransform.size() != ven::enigma::letterSizeT)
	{
		throw std::exception("Invalid input");
	}
	if (turnOver.size() == 0) {
		// Possible warning or exception
		_turnOverPositions.insert(ven::enigma::Z);
	}
	else {
		for (const auto& c : transform)
		{
			_turnOverPositions.insert(get(c));
		}
		// If repeated, cauld launch an exception, warning, or whatever
		/*
		if (_turnOverPositions.size() != turnOver.size())
		{
			// Possible warning or exception
		}
		*/
	}
}
ven::enigma::Rotor::Rotor(const char* transform, const char* turnOver) :
	Rotor(std::string(transform), std::string(turnOver))
{}

ven::enigma::letter ven::enigma::Rotor::transformFoward(const ven::enigma::letter input) const
{
	return internalTransformFoward(input + _internalPosition) - _internalPosition;
}
ven::enigma::letter ven::enigma::Rotor::transformBackward(const ven::enigma::letter input) const
{
	return internalTransformBackward(input + _internalPosition) - _internalPosition;
}

bool ven::enigma::Rotor::advance()
{
	bool res = isAtTurnOverPosition();
	++_internalPosition;
	return res;
}
bool ven::enigma::Rotor::isAtTurnOverPosition() const
{
	if (_turnOverPositions.find(_internalPosition) != _turnOverPositions.end())
	{
		return true;
	}
	return false;
}

//ven::enigma::Rotor::~Rotor(){}
